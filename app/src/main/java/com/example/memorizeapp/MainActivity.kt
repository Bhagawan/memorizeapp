package com.example.memorizeapp

import android.graphics.Bitmap
import android.graphics.drawable.BitmapDrawable
import android.os.Bundle
import android.view.View
import android.widget.FrameLayout
import android.widget.LinearLayout
import android.widget.TextView
import androidx.appcompat.widget.AppCompatButton
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.view.WindowCompat
import androidx.core.view.WindowInsetsCompat
import androidx.core.view.WindowInsetsControllerCompat
import com.example.memorizeapp.game.Game
import com.example.memorizeapp.mvp.GamePresenter
import com.example.memorizeapp.mvp.GamePresenterViewInterface
import moxy.MvpAppCompatActivity
import moxy.presenter.InjectPresenter

class MainActivity : MvpAppCompatActivity(), GamePresenterViewInterface {

    @InjectPresenter
    lateinit var mPresenter: GamePresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        fullscreen()
        mPresenter.initialize(this, findViewById(R.id.game))
        val newGameButton : AppCompatButton = findViewById(R.id.btn_new_game)
        newGameButton.setOnClickListener { mPresenter.newGame() }
    }

    private fun fullscreen() {
        WindowInsetsControllerCompat(window, window.decorView).let { controller ->
            controller.hide(WindowInsetsCompat.Type.systemBars())
            controller.systemBarsBehavior = WindowInsetsControllerCompat.BEHAVIOR_SHOW_TRANSIENT_BARS_BY_SWIPE
        }
        WindowCompat.setDecorFitsSystemWindows(window, false)
    }

    override fun showGame() {
        val menuLayout : ConstraintLayout = findViewById(R.id.layout_menu)
        val gameLayout : ConstraintLayout = findViewById(R.id.layout_game)
        val headerView : TextView = findViewById(R.id.text_game_header)
        val mistakesLayout : LinearLayout = findViewById(R.id.layout_game_mistakes)
        menuLayout.visibility = View.GONE
        gameLayout.visibility = View.VISIBLE
        headerView.visibility = View.VISIBLE
        mistakesLayout.visibility = View.VISIBLE
    }

    override fun showMenu() {
        val menuLayout : ConstraintLayout = findViewById(R.id.layout_menu)
        val headerView : TextView = findViewById(R.id.text_game_header)
        val mistakesLayout : LinearLayout = findViewById(R.id.layout_game_mistakes)
        menuLayout.visibility = View.VISIBLE
        headerView.visibility = View.INVISIBLE
        mistakesLayout.visibility = View.INVISIBLE
    }

    override fun setHeader(stage: Boolean) {
        val headerView : TextView = findViewById(R.id.text_game_header)
        headerView.text = when(stage) {
            Game.REPEAT -> getString(R.string.msg_repeat)
            else -> getString(R.string.msg_memorize)
        }
    }

    override fun setMistakes(amount: Int) {
        val mistakesView : TextView = findViewById(R.id.text_mistakes_amount)
        mistakesView.text = amount.toString()
    }

    override fun setRecord(amount: Int) {
        val recordView : TextView = findViewById(R.id.text_record)
        recordView.text = amount.toString()
    }

    override fun setCurrentResult(result: Int) {
        val resultLayout : LinearLayout = findViewById(R.id.layout_current_result)
        resultLayout.visibility = View.VISIBLE
        val resultView : TextView = findViewById(R.id.text_result)
        resultView.text = result.toString()
    }

    override fun setBackground(back: Bitmap) {
        val mainLayout : FrameLayout = findViewById(R.id.layout_main)
        mainLayout.background = BitmapDrawable(resources, back)
    }


}