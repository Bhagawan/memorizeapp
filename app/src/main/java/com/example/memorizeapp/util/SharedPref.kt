package com.example.memorizeapp.util

import android.content.Context

class SharedPref {
    companion object {
        fun setRecord(context : Context, record : Int) {
            val shP = context.getSharedPreferences("Record", Context.MODE_PRIVATE)
            shP.edit().putInt("record", record).apply()
        }

        fun getRecord(context : Context) : Int{
            val shP = context.getSharedPreferences("Record", Context.MODE_PRIVATE)
            return shP.getInt("record", 0)
        }

    }
}