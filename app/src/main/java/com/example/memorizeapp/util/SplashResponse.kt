package com.example.memorizeapp.util

import androidx.annotation.Keep

@Keep
data class SplashResponse(val url : String)

