package com.example.memorizeapp.mvp

import android.content.Context
import android.graphics.Bitmap
import android.graphics.drawable.Drawable
import com.example.memorizeapp.game.Game
import com.example.memorizeapp.util.SharedPref
import com.squareup.picasso.Picasso
import com.squareup.picasso.Target
import moxy.InjectViewState
import moxy.MvpPresenter
import java.lang.Exception

@InjectViewState
class GamePresenter : MvpPresenter<GamePresenterViewInterface>() {
    private lateinit var game: Game
    private lateinit var target : Target

    override fun onFirstViewAttach() {
        target = object : Target {
            override fun onBitmapLoaded(bitmap: Bitmap?, from: Picasso.LoadedFrom?) {
                bitmap?.let{ viewState.setBackground(bitmap) }
            }

            override fun onBitmapFailed(e: Exception?, errorDrawable: Drawable?) {}
            override fun onPrepareLoad(placeHolderDrawable: Drawable?) {}
        }
        Picasso.get().load("http://195.201.125.8/MemorizeApp/back.png").into(target)
    }

    fun initialize(context : Context, game : Game) {
        viewState.setRecord(SharedPref.getRecord(context))
        this.game = game
        game.setCallback(object : Game.Callback {
            override fun setStage(stage: Boolean) {
                viewState.setHeader(stage)
            }

            override fun endGame(level: Int) {
                if(SharedPref.getRecord(context) < level) {
                    SharedPref.setRecord(context, level)
                    viewState.setRecord(level)
                }
                viewState.showMenu()
                viewState.setCurrentResult(level)
            }

            override fun mistakes(amount: Int) {
                viewState.setMistakes(amount)
            }

        })
    }

    fun newGame() {
        viewState.showGame()
        game.newGame()
    }
}