package com.example.memorizeapp.mvp

import android.graphics.Bitmap
import moxy.MvpView
import moxy.viewstate.strategy.alias.OneExecution
import moxy.viewstate.strategy.alias.SingleState

interface GamePresenterViewInterface : MvpView {

    @OneExecution
    fun showGame()

    @OneExecution
    fun showMenu()

    @OneExecution
    fun setHeader(stage : Boolean)

    @OneExecution
    fun setMistakes(amount : Int)

    @OneExecution
    fun setRecord(amount : Int)

    @OneExecution
    fun setCurrentResult(result : Int)

    @SingleState
    fun setBackground(back : Bitmap)

}