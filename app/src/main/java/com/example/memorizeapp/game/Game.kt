package com.example.memorizeapp.game

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Canvas
import android.graphics.Paint
import android.os.Handler
import android.os.Looper
import android.util.AttributeSet
import android.view.MotionEvent
import android.view.View
import androidx.core.content.res.ResourcesCompat
import com.example.memorizeapp.R


class Game(context: Context?, attrs: AttributeSet?) : View(context, attrs) {

    companion object {
        const val MEMORIZE = true
        const val REPEAT = false
    }
    private var mWidth = 0
    private var mHeight = 0
    private var amount  : Int = 3
    private var squareSize = 75f
    private var interval = 10f
    private var topOffset = 0f
    private var leftOffset = 0f
    private var level = 1
    private var currentStage = true
    private val points = ArrayList<Pair<Int, Int>>()
    private val checkedPoints = ArrayList<Pair<Int, Int>>()
    private var callback: Callback? = null
    private var mistakes = 5


    override fun onLayout(changed: Boolean, left: Int, top: Int, right: Int, bottom: Int) {
        super.onLayout(changed, left, top, right, bottom)
        if(height > 0) {
            mHeight = height
            mWidth = width
            calculateSize()
            invalidate()
        }
    }

    override fun onDraw(canvas: Canvas?) {
        if(mHeight > 0 && canvas != null) {
            drawField(canvas)
        }
    }

    @SuppressLint("ClickableViewAccessibility")
    override fun onTouchEvent(event: MotionEvent?): Boolean {
        if(currentStage == REPEAT) {
            return when (event?.action) {
                MotionEvent.ACTION_DOWN -> true
                MotionEvent.ACTION_UP -> {
                    val p = getPointOnCoords(event.x, event.y)
                    p?.let{
                        if(!checkedPoints.contains(p)) {
                            checkedPoints.add(it)
                            checkPoint(p)
                            invalidate()
                        }
                    }
                    true
                }
                else -> super.onTouchEvent(event)
            }
        }
        return super.onTouchEvent(event)
    }

    fun setCallback(callback: Callback) {
        this.callback = callback
    }

    private fun drawField( c : Canvas) {
        c.drawColor(ResourcesCompat.getColor(context.resources, R.color.game_back, null))
        val p = Paint()
        for(i in 0 until amount) {
            for(j in 0 until amount) {
                if(currentStage == MEMORIZE && points.contains(Pair(i,j))) {
                    p.style = Paint.Style.FILL
                    p.color = ResourcesCompat.getColor(context.resources, R.color.white, null)
                } else if(checkedPoints.contains(Pair(i, j))) {
                    p.style = Paint.Style.FILL
                    p.color = ResourcesCompat.getColor(context.resources, R.color.white, null)
                } else {
                    p.style = Paint.Style.STROKE
                    p.strokeWidth = 3f
                    p.color = ResourcesCompat.getColor(context.resources, R.color.game_square_borders, null)
                }
                c.drawRect(leftOffset + squareSize * i + (interval * i), topOffset + squareSize * j + (interval * j ),
                    leftOffset + squareSize * (i + 1) + (interval * i), topOffset + squareSize * (j + 1) + (interval * j ), p)
            }
        }
    }

    fun newGame() {
        mistakes = 5
        level = 1
        amount = 3
        callback?.mistakes(mistakes)
        startLevel()
    }

    private fun startLevel() {
        currentStage = MEMORIZE
        checkedPoints.clear()
        callback?.setStage(currentStage)
        calculateSize()
        generatePattern()
        invalidate()
        val handler = Handler(Looper.getMainLooper())
        handler.postDelayed({
            currentStage = REPEAT
            callback?.setStage(currentStage)
            invalidate()
        }, 5000)
    }

    private fun calculateSize() {
        val max = when(width < height) {
            true -> width
            else -> height
        }
        interval = (30 / amount).toFloat()
        squareSize = (max - interval * amount) / amount
        topOffset = (height - squareSize * amount - (interval * (amount - 1))) / 2
        leftOffset = (width - squareSize * amount - (interval * (amount - 1))) / 2
    }

    private fun generatePattern() {
        points.clear()
        var a = (amount * 1.5).toInt()
        while(a > 0) {
            val p = Pair((0 until amount).random(), (0 until amount).random())
            if(!points.contains(p)) {
                points.add(p)
                a--
            }
        }
    }

    private fun checkPoint(p : Pair<Int, Int>) {
        if(!points.contains(p)) {
            mistakes--
            callback?.mistakes(mistakes)
            if(mistakes <= 0) callback?.endGame(level)
        } else {
            var f = true
            for(point in points) if(!checkedPoints.contains(point)) f = false
            if(f) {
                level++
                amount = 3 + level / 2
                startLevel()
            }
        }
    }

    private fun getPointOnCoords(x : Float, y : Float) : Pair<Int, Int>? {
        val pX : Int = ((x - leftOffset) / (squareSize + interval)).toInt()
        val pY : Int = ((y - topOffset) / (squareSize + interval)).toInt()
        if(pX in 0 until amount && pY in 0 until amount) return Pair(pX, pY)
        return null
    }

    interface Callback {
        fun setStage(stage : Boolean)
        fun endGame(level : Int)
        fun mistakes(amount : Int)
    }
}